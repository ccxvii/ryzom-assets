blend_to_iqe *.blend

for f in *_visage.iqe
do
	mv $f $f.orig
done

for f in *_hof_visage.iqe.orig
do
	iqe-copy-bind-pose ge_hof_skel.iqe < $f | sed 's/_Shape_visage//;s/_Basis//' | iqe-split
done

for f in *_hom_visage.iqe.orig
do
	iqe-copy-bind-pose ge_hom_skel.iqe < $f | sed 's/_Shape_visage//;s/_Basis//' | iqe-split
done
